/**
 * tzw 2022年7月5日20:00:14
 */
// 严格模式
'use strict'

// 引入配置文件（系统基础配置）
const config = require('../config/app.config')

const opener = require('opener')
const fs = require('fs')
const path = require('path')

// Koa框架
const Koa = require('koa')

// Koa实例化出来一个application
const app = new Koa()

app.use(
    require('koa-static')(config.WEBPACK_CLIENT_ROOT, {
        maxage: 31536000000
    })
)

app.use(async (ctx, next) => {
    // 忘记原因了，以后再加上吧，应该是为了防止某个错误
    //   if (ctx.request.url.substring(ctx.request.url.length - 4) === '.css') {
    //     ctx.response.type = 'text/html'
    //     ctx.response.status = 404
    //     return await next()
    //   }

    ctx.response.type = 'html'

    ctx.response.body = fs.createReadStream(
        path.join(config.WEBPACK_CLIENT_ROOT, config.HTML_NAME)
    )

    await next()
})

// 监听端口
app.listen(config.PORT, () => {
    console.log(`正在监听端口：${config.PORT}`)
    opener(`http://127.0.0.1:${config.PORT}`)
})
