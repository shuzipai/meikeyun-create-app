/**
 * tzw 2017年8月29日14:32:23
 */
/*global module require */
/*eslint no-console: "off" */
// 严格模式
'use strict'

// 引入配置文件（系统基础配置）
const config = require('../config/app.config')

const fs = require('fs')
const path = require('path')

const webpack = require('webpack')

// 打包配置
const webpack_client_config = require(`${config.CONFIG_ROOT}/webpack.config.js`)


// SSR模式下打包双端代码 普通模式下只打包前端代码
webpack(config.SSR ? webpack_client_config : webpack_client_config[1], (err, stats) => {
  if (err) {
    console.error(err.stack || err);
    if (err.details) {
      console.error(err.details);
    }
    return;
  }

  const info = stats.toJson();

  if (stats.hasErrors()) {
    console.error(info.errors);
  }

  if (stats.hasWarnings()) {
    console.warn(info.warnings);
  }

  // 普通模式下生成index.html文件
  if (!config.SSR) {
    const Env = require('./node_app_render/nunjucksEnv')
    const client_stats = require('../webpack_client/loadable-stats.json')
    
    const html = Env.render('index.html', {
      script_tags:
        [...client_stats.assetsByChunkName.app, ...(client_stats.assetsByChunkName.vendor || [])].map(name => {
          if (name.endsWith('.css')) {
            return `<link rel="stylesheet" href="/${name}">`
          }
          return `<script src="/${name}"></script>`
        }).join('')
    })

    fs.writeFileSync(
      path.join(config.WEBPACK_CLIENT_ROOT, config.HTML_NAME),
      html
    )
  }

  console.log(stats.toString({ colors: true }))

  process.exit(0)
})