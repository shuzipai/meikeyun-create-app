import React from 'react'

const loadableRouter = router =>
  Object.keys(router)
    .sort((a, b) => b.length - a.length)
    .map(key => {
      const Com = router[key]
      return {
        path: key,
        element: <Com />
      }
    })

export { loadableRouter }
