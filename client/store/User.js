import { makeAutoObservable } from "mobx"

export default class User {
    // 用户凭证
    token = ""

    constructor() {
        makeAutoObservable(this)
        setTimeout(() => {
            this.token = '过了3秒开始有值'
        }, 3000)
    }
}
