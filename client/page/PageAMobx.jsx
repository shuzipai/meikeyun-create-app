import React from 'react'
import storeContext from '../context/storeContext'
// 只有在class下才使用mobx-react
import { observer } from 'mobx-react';

class PageA extends React.Component {
    static contextType = storeContext

    render() {
        const { user } = this.context

        return (
            <div>
                <h1>用户的token:{user.token}</h1>
            </div>
        )
    }
}

export default observer(PageA)
