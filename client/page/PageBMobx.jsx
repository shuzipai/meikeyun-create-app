import React from 'react'
import { observer } from 'mobx-react-lite'
import styles from './styles.scss'
import { useStore } from '../hooks/storeHooks'

export default observer(function pageB() {
  const { user } = useStore()

  return <div className={styles['try']}>用户的token:{user.token}</div>
})
