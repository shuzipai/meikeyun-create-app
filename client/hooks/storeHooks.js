import { useContext } from "react"
import storeContext from "../context/storeContext"
import { observer } from "mobx-react-lite"

function useStore() {
    return useContext(storeContext)
}

export {
    observer,
    useStore
}
